﻿[<RequireQualifiedAccess>]
module DFlex.DFlexSerializer

open System
open DFlex.TextImport
open DFlex.DataExport
open DFlex.DataImport
open DFlex.TextExport

let SerializeWith<'T> setts =
    let t = typeof<'T>
    if t = typeof<obj> then null else t
    |> parseData
    >> convertText setts
    >> if setts.indentSpaces <> null then indenter setts.indentSpaces else fun a -> a
    
let Serialize<'T> =
    SerializeWith DFlexSettings.Default

let Serialize' : obj -> string =
    parseData null >> convertText DFlexSettings.Default
    
let Deserialize<'T> =
    parseText
    >> convertData typeof<'T>
    >> function
    | :? 'T as t -> t
    | _ -> Unchecked.defaultof<'T>
    
let Deserialize' (a: string, t: Type) =
    parseText a
    |> convertData t

let tab_space_indent = "\t"
let two_space_indent = String.replicate 2 " "
let four_space_indent = String.replicate 4 " "
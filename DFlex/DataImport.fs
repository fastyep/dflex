﻿module DFlex.DataImport

open System
open System.Collections
open System.Reflection
    
type DConverter = Result<DObject, Type * obj>
type DConverterFunc = DConverter -> DConverter
    
let mutable private converters : DConverterFunc list = []

let useConverter (converter: Type * _ -> DObject option) =
    converters <-
    function
    | Ok a -> Ok a
    | Error a ->
        converter a
        |> Option.map Ok
        |> Option.defaultValue (Error a)
    :: converters

let parseData (t: Type) (o: _) =
    if o = null then Error (t, o) else
        let t = if t = null then o.GetType() else t
        converters
        |> Seq.reduce (>>)
        <| Error (t, o)
    |> function Ok a -> a | Error _ -> DObject.CreateValue None null
    
useConverter <| fun (t, o) ->
    let props =
        t.GetProperties(BindingFlags.Public ||| BindingFlags.Instance)
        |> Seq.map (fun a -> a.Name, a.PropertyType, a.GetValue)
    t.GetFields(BindingFlags.Public ||| BindingFlags.Instance)
    |> Seq.map (fun a -> a.Name, a.FieldType, a.GetValue)
    |> Seq.append props
    |> Seq.map (fun (name, t, getter) ->
        let result = parseData t <| getter o
        { result with key = Some <| DObject.CreateValue None name })
    |> Seq.toList
    |> DObject.CreateDict None
    |> Some
    
useConverter <| fun (t, o) ->
    let naming = t.FullName.Split '.'
    if naming.Length = 2 && naming.[0] = "System" || t.IsEnum then
        o.ToString()
        |> DObject.CreateValue None
        |> Some else
    None
    
useConverter <| fun (t, o) ->
    if t.GetInterface "System.Collections.IList" <> null then
        let el = t.GetElementType()
        let el = if el = null then t.GetGenericArguments().[0] else el
        o :?> IEnumerable
        |> Seq.cast<_>
        |> Seq.mapi (fun i a -> { parseData el a with key = string i |> DObject.CreateValue None |> Some } )
        |> Seq.toList
        |> DObject.CreateList None
        |> Some else
    None
    
useConverter <| fun (t, o) ->    
    if t.GetInterface "System.Collections.IDictionary" <> null then        
        o :?> IDictionary
        |> Seq.cast<_>
        |> Seq.map (fun el ->
            let tel = el.GetType()
            let tkey = tel.GetProperty "Key"
            let key = tkey.GetValue el
            let tvalue = tel.GetProperty "Value"
            let value = tvalue.GetValue el
            { parseData tvalue.PropertyType value with key = Some <| parseData tkey.PropertyType key } )
        |> Seq.toList
        |> DObject.CreateDict None
        |> Some else
    None
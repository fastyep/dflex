﻿namespace DFlex

open System

type DKind =
    | unknown = 0uy
    | value = 1uy
    | dict = 2uy
    | list = 3uy

type DObject =
    {
        kind: DKind
        key: DObject option
        value: string
        child: DObject list
    }
    member o.KeyName =
        o.key
        |> Option.map (fun a -> if a.kind = DKind.value then a.value else null)
        |> Option.defaultValue null
    member o.GetKey key =
        if o.kind <> DKind.dict then failwith ""
        o.child
        |> Seq.tryFind (fun a -> a.key = key)
    member o.GetItem index =
        if o.kind <> DKind.list then failwith ""
        o.child
        |> Seq.tryFind (fun a -> a.KeyName = string index)
    member o.GetValue (t: Type) =
        if o.kind <> DKind.value then failwith ""
        Convert.ChangeType(o.value, t)
    member o.GetValue<'T> () =
        match o.GetValue typeof<'T> with
        | :? 'T as a -> a
        | _ -> Unchecked.defaultof<'T>
    static member CreateValue key value =
        { kind = DKind.value
          key = key
          value = value
          child = [] }
    static member CreateDict key fields =
        { kind = DKind.dict
          key = key
          value = null
          child = fields }
    static member CreateList key elements =
        { kind = DKind.list
          key = key
          value = null
          child = elements }
﻿module DFlex.TextImport

open System.Collections.Generic
open System.Text.RegularExpressions

let private splitter' (separator: string) (text: string) =
    let mutable vector = 0
    let mutable last = text.Length
    let mutable oq = None
    let mutable mode = false
    seq {
        for i in text.Length - 1 .. -1 .. -1 ->
        if i = -1 then
            if i = 0 && vector <> 0 then failwith ""
            text.Substring(0, last)
        else
        let c = text.[i]
        if c = '"' then
            mode <- (mode && i > 0 && text.[i-1] <> '\\') |> not
            null else
        if mode then null else
        if c = '{' then vector <- vector + 1
        if c = '}' then vector <- vector - 1
        if separator.Contains c && vector = 0 then
            oq <- Some c
            let a = text.Substring(i + 1, last - i - 1)
            last <- i
            a
        else null }
    |> Seq.filter (isNull >> not)
    |> Seq.rev
    |> Seq.toList, oq
    
let private splitter a = splitter' a >> fst

let parseText (text: string) =
    let tkn = text.Trim()
    if tkn = "" then DObject.CreateList None [] else
    let extract (key: string) =
        if key.StartsWith '"' || key.EndsWith '"' then
            Regex.Unescape <| key.Substring(1, key.Length - 2)
        else key
    let subi = string >> DObject.CreateValue None >> Some
    let rec sub key (token: string) =
        let token = token.Trim()
        if token = "" then DObject.CreateValue key null else
        if token.StartsWith '{' || token.EndsWith '}' then
            let token = token.Substring(1, token.Length - 2).Trim()
            if token.Length > 0 then
                splitter ";" token
                |> List.map (fun field ->
                    let key, values, empty =
                        let parts, del = splitter' ":=" field
                        let key = parts.[0] |> sub None |> Some
                        del
                        |> Option.map (fun del -> key, parts.[1], if del = '=' then None else Some false )
                        |> Option.defaultValue (key, null, Some true)
                    if empty.IsSome then
                        if empty.Value then [] else
                            values
                            |> splitter ","
                            |> List.mapi (subi >> sub)
                        |> DObject.CreateList key
                    else sub key values)
                |> DObject.CreateDict key
            else DObject.CreateValue key token
        else DObject.CreateValue key <| extract token
    if tkn.[0] = ':' then
        tkn.Substring 1
        |> splitter "," 
        |> List.mapi (subi >> sub)
        |> DObject.CreateList None else
    let sub = sub None
    if tkn.[0] = '=' then sub <| tkn.Substring(1) else
    sub <| sprintf "{%s}" tkn
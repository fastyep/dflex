﻿namespace DFlex

type DFlexSettings =
    {
        forcedEscape: bool
        indentSpaces: string
        ignoreNullFields: bool
    }
    static member Default =
        { indentSpaces = null
          forcedEscape = false
          ignoreNullFields = false }
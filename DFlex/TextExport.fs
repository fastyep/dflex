﻿module DFlex.TextExport

open System
open System.Linq

let private token_sequences = ":={};"
let private escape_sequences = "\\\"\'\a\b\f\n\r\t\v"
let private escaped_sequences = "\\\"\'abfnrtv"

let private needEscape value =
    String.IsNullOrEmpty value
    || value.Trim() <> value
    || value.Intersect(token_sequences + escape_sequences).Count() > 0
    
let private escape (value: string) =
    if value = null then "" else
    let esca (a: char) =
        let ind = escape_sequences.IndexOf a
        if ind = -1 then string a else
        escaped_sequences.[ind]
        |> sprintf "\\%c"
    value
    |> Seq.map esca
    |> String.concat ""
    |> sprintf "\"%s\""

let convertText (setts: DFlexSettings) root =
    let getSep ch =
        if ch.kind = DKind.list then
            if ch.child.Length > 0 then sprintf "%s:"
            else string
        else sprintf "%s="
    let rec sub =
        function
        | { kind = DKind.dict; child = child } ->
            child
            |> Seq.filter (fun a -> not setts.ignoreNullFields || a.kind <> DKind.value || a.value <> null)
            |> Seq.map (fun ch ->
                ch.key
                |> Option.map (sub >> getSep ch)
                |> Option.defaultValue ""
                |> sprintf "%s%s"
                <| sub ch)
            |> String.concat (if setts.indentSpaces <> null then ";\n" else ";")
            |> sprintf (if setts.indentSpaces <> null then "{\n%s\n}" else "{%s}")
        | { kind = DKind.list; child = child } ->
            child
            |> Seq.map sub
            |> String.concat ","
        | { kind = DKind.value; value = v } -> if setts.forcedEscape || needEscape v then escape v else v
        | _ -> ""
    sprintf "%s%s"
    <| getSep root ""
    <| sub root
    
let indenter (tab: string) (text: string) =
    let mutable vector = 0
    text.Split '\n'
    |> Seq.map (fun x ->
        if x.[0] = '}' then vector <- vector - 1
        let ret = String.replicate vector tab + x
        if x.[x.Length-1] = '{' then vector <- vector + 1        
        ret)
    |> String.concat "\n"
﻿module DFlex.DataExport

open System
open System.Collections.Generic
open System.Reflection
open FSharp.Reflection
    
type DParser = Result<obj, Type * DObject>
type DParserFunc = DParser -> DParser
    
let mutable private parsers : DParserFunc list = []

let useParser (parser: Type * DObject -> obj option) =
    parsers <-
    function
    | Ok a -> Ok a
    | Error a ->
        parser a
        |> Option.map Ok
        |> Option.defaultValue (Error a)
    :: parsers

let convertData (t: Type) (token: DObject) =
    parsers
    |> Seq.reduce (>>)
    <| Error (t, token)
    |> function
    | Ok a -> a
    | Error _ -> null
    
useParser <| fun (t, token) ->
    if token.kind <> DKind.value then None else
    if token.value = null then Some null else
    if t.IsEnum then Enum.Parse(t, token.value) |> Some else
    if t.GetInterface "System.IConvertible" <> null then
        try Convert.ChangeType(token.value, t) with _ -> null
        |> Some else
    t.GetMethods(BindingFlags.Public ||| BindingFlags.Static)
    |> Seq.tryFind (fun a ->
        let prms = a.GetParameters()
        a.Name = "Parse" && prms.Length = 1 && prms.[0].ParameterType = typeof<string>)
    |> Option.map (fun a -> try a.Invoke(null, [|token.value|]) with _ -> null)
    
useParser <| fun (t, token) ->
    if token.kind <> DKind.dict then None else
    let t =
        if t.IsGenericType && t.GetGenericTypeDefinition() = typedefof<IDictionary<_,_>> then
            let types = t.GetGenericArguments()
            typedefof<Dictionary<_,_>>.MakeGenericType types
        else t
    if t.GetConstructor([||]) = null then
        t.GetConstructors()
        |> Seq.sortBy (fun a -> a.GetParameters().Length)
        |> Seq.tryLast
        |> Option.map (fun constr ->
            constr.GetParameters()
            |> Array.map (fun prm ->
                token.child
                |> Seq.tryFind (fun a -> a.KeyName = prm.Name || a.KeyName.ToLower() = prm.Name.ToLower())
                |> Option.map (convertData prm.ParameterType)
                |> Option.defaultValue null)
            |> constr.Invoke) else
    match Activator.CreateInstance t with
    | :? System.Collections.IDictionary as dic ->
        let types = t.GetGenericArguments()
        token.child
        |> Seq.iter (fun f -> dic.[convertData types.[0] f.key.Value] <- convertData types.[1] f)
        dic :> obj
    | a ->
        token.child
        |> Seq.iter (fun f ->
            t.GetFields(BindingFlags.Public ||| BindingFlags.Instance)
            |> Seq.tryFind (fun x -> not x.IsInitOnly && x.Name.ToLower() = f.KeyName.ToLower())
            |> function
            | Some field -> field.SetValue(a, convertData field.FieldType f)
            | None ->
                t.GetProperties(BindingFlags.Public ||| BindingFlags.Instance)
                |> Seq.tryFind (fun x -> x.CanWrite && x.Name.ToLower() = f.KeyName.ToLower())
                |> function
                | Some prop -> prop.SetValue(a, convertData prop.PropertyType f)
                | None -> ())
        a
    |> Some
    
useParser <| fun (t, token) ->
    if token.kind <> DKind.list then None else
    if t.IsArray then
        let el = t.GetElementType()
        let arr = Array.CreateInstance(el, token.child.Length)
        Array.Copy(token.child |> Seq.map(fun x -> convertData el x) |> Seq.toArray, arr, arr.Length)
        arr :> obj |> Some else
    None
    
useParser <| fun (t, token) ->    
    if t.IsGenericType && t.GetGenericTypeDefinition() = typedefof<Nullable<_>> then
        convertData (t.GetGenericArguments().[0]) token |> Option.ofObj else
    if t.IsGenericType && t.GetGenericTypeDefinition() = typedefof<Option<_>> then
        let typey = t.GetGenericArguments().[0]
        let cases = 
            typeof<unit option>.GetGenericTypeDefinition().MakeGenericType [|typey|]
            |> FSharpType.GetUnionCases
            |> Array.partition (fun x -> x.Name = "Some")
        let relevantCase, args =
            match convertData typey token with
            | null -> snd cases |> Array.exactlyOne, [| |]
            | v -> fst cases |> Array.exactlyOne, [| v |]
        FSharpValue.MakeUnion(relevantCase, args) |> Some else
    None
﻿open System
open System.Collections.Generic
open System.Drawing
open System.Text.Json
open DFlex
open DFlex.TextImport

type [<CLIMutable>] SimpleTest =
    {
        id: int
        name: string
        uid: Guid
        friends: int array
    }
    
type Numpad =
    | zero = 0uy
    | one = 1uy
    | ustal = 2uy
    
type HardTest =
    {
        id: int
        name: string
        uid: Guid
        friends: int array
        key: Numpad
    }
    
type Name<'T>(name: string, id: 'T option) =
    override __.ToString() =
        sprintf "%s\t%A" name id

[<EntryPoint>]
let main argv =
    let t = DFlexSerializer.Deserialize<HardTest> "id=1;name=\"t{e}st\";uid=61c54bbd-c2c6-5271-96e7-009a87ff44bf;friends:3,5,7"
//    let r = DFlexSerializer.Deserialize<IDictionary<Point, string>> "{x=2;y=3}=\"\""
//    Name<DateTime>("Seychas:", Some DateTime.Now)
//    dict ["lol", 2]
//    |> DataImport.parseData null
//    Name("Eblan", Some 1)
    let dic = Dictionary<Point, HardTest>()
    dic.[Point(69,69)] <- { t with name = null }
    dic
    |> DFlexSerializer.SerializeWith
           { DFlexSettings.Default with
               indentSpaces = DFlexSerializer.four_space_indent
               ignoreNullFields = true }
    |> printfn "%s"
    0 // return an integer exit code

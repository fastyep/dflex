# Dflex
## About
DFlex is JSON like language for compact data store, convenient usage in api or configs and human readable syntax. Main goal of this project - make new protocol, better than JSON and replace it step-by step.
### Roadmap
- [x] Make first realization on .NET platform
- [x] Write documentation of this protocol
- [ ] Write library for other languages like JavaScript or Go
### Why DFlex is better than JSON?
Lets start with example:
```json
{
	"key" : "value",
	"array" : [14, 88],
	"obj": {
		"nil" : null
	}
}
```
And this is how same JSON data looks in DFlex:
```
={
	"key" = "value";
	"array" : 14, 88;
	"obj" = {
		"nil"=;
	}
}
```
But wait! In DFlex you can store keys and values without quotes - it becomes more human readable and compact:
```
={
	key= value;
	array: 14, 88;
	obj= {
		nil=;
	}
}
```
And thats not all) If root element is KeyValue object you can skip  brackets:
```
key= value;
array: 14,88;
obj={
	nil=;
}
```
Imagine how JSON-configs becomes more clear and JSONAPI response shorter:
```
{"key":"value","array":[14,88],"obj":{"nil":null}}
vs
key=value;array:14,88;obj={nil=;}
```